# Alice skill Time Tracking
---

```
git clone [repo]
```


## Install dependencies:
```
pip install -r requirements.txt
```
---

## Build
```
make build
```

## Run
```
make run
```

## Test
```
make test
```

Skill for ALice

Time Tracking
