VERSION = '0.1.5a'

from app.helper import getenv

def info():
    return f'''
    Time Tracking [Alice's skill]:
    > version: {VERSION}
    > user: {getenv('LOGNAME')}
    > save_file: {getenv('TT_SAVE_JSON')}
    '''
