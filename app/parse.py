''' 
For parsing incoming phrases and activating actions
And for representing the answer in human readable format
'''


import logging
import random
import re

from app.helper import about, help_for_user, panic_phrases
from app.tracking import delta_time, pause_time, save_time

all_phrases = {
    "Start": {
        "init_phrase": ["начало", "старт", "поехали", "запусти/запускай", "засеки/засекай"],
        "reg": [x for x in [r"нач", r"старт", r"поехали", r"запус", r"засек"]],
        "function": save_time
    },
    "Pause": {
        "init_phrase": ["пауза/поставить на паузу"],
        "reg": [r'пауз'],
        "function": pause_time
    },
    "Delta": {
        "init_phrase" : ["время", "сколько", "выведи/выводи", "покажи/показывай"],
        "reg": [x for x in [r'врем', r'сколько', r'выведи', r'покаж', r'показывай', r'вывод']],
        "function": delta_time
    },
    "Help": {
        "init_phrase" : ["помощь/помоги"],
        "reg": [x for x in [r'помо']],
        "function": help_for_user
    },
    "About": {
        "init_phrase" : ["справка"],
        "reg" : [x for x in [r'справк']],
        "function" : about
    }
}

def parse_phrase(user_id, text):
    '''Parse phrase (txt) that'l come from user
    Should activate the right action and return answer for user

    :type user_id: str\n
    :type text: str
    :rtype: str
    '''
    
    logging.info(f'Request for parse_phrase(user_id = {user_id}, text = {text}) was received') 
    
    text = text.lower()
    for act in all_phrases:
        for reg in all_phrases[act]["reg"]:
            reg_phrases = re.search(reg, text)
            if reg_phrases: 
                logging.info(f'Regex "{reg}" was used')
                return all_phrases[act]["function"](user_id)

    logging.warning(f'No right phrases in parse_phrase(user_id = {user_id}, text = {text})') 
    return random.choice(panic_phrases)
