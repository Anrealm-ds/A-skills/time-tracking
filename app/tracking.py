'''Track the time
Save, delta, del
'''

from app import helper
import time
import random
import logging

from app.helper import pause_phrases
from app.helper import load_jsond_from_file, getenv
from app.helper import save_jsond_to_file
from app.helper import decsec

TT_SAVE_JSON = getenv('TT_SAVE_JSON')
if TT_SAVE_JSON is None:
    users = {} # as db; keep some info for user by user_id
else:
    users = load_jsond_from_file(TT_SAVE_JSON)

def save_time(user_id):
    '''Save timestamp for user'''
    users[user_id] = time.time()
       
    logging.info( f'Request for save_time(user_id = {user_id}) was received')
    if TT_SAVE_JSON:
        logging.info("Writed!")
        save_jsond_to_file(users, TT_SAVE_JSON)

    return random.choice(helper.phrases_first)


def delta_time(user_id):
    '''Delta time from last call save_time for user
    return the number of seconds since last call
    :type user_id: str
    return (text, tts)
    '''
   
    if (users.get(user_id) is None):   
        logging.warning( f'User_id does not exist. Time is not running now.') 
        return random.choice(helper.phrases_no_user)

    dtime = time.time() - users[user_id]
    
    dseconds = int(dtime)
    days = dseconds // 86400
    hours = (dseconds - days * 86400) // 3600
    minutes = (dseconds - days * 86400 - hours * 3600) // 60
    seconds = dseconds - days * 86400 - hours * 3600 - minutes * 60

    result = "Прошло: " + (f"{days} дн. " if days else "") + \
    (f"{hours} ч. " if hours else "") + \
    (f"{minutes} мин. " if minutes else "") + \
    (f"{seconds} с. " if seconds else "")

    logging.info( f'Request for delta_time(user_id = {user_id}) was received')
    return (result, result.replace('с.', decsec(seconds)))


def pause_time(user_id):
    '''Pauses the stopwatch. 
    After need again use "start"
    '''
    return random.choice(pause_phrases)
