import json
import logging
import random

from flask import Flask, request

from app import parse
from app.helper import meeting, old_meeting, get_suggests
from app.tracking import users
from app import info

app = Flask(__name__)

@app.route("/", methods=['GET'])
def greeting():
    """
    Test func, TODO remove in production
    :rtype: string
    """

    return info().replace('\n', '<br>')

@app.route("/", methods=["POST"])
def main():
    """
    Get JSON-request from Alice and send answer back
    :return: json-response with time or text
    :rtype: JSON 
    """

    response = {
        "version": "",
        "session": {},
        "response": {
            "text": "",
            "buttons": get_suggests(),
            "end_session": False
        }
    }

    # TODO add validation of request data
    try:
        response["session"] = request.json["session"]
        response["version"] = request.json["version"]

    except:
        logging.error(f'Such JSON-request does not valid')
        response['response']["text"] = "Это как-то неправильно..."
        return json.dumps(
            response,
            ensure_ascii=False,
            indent=2
        )

    user_id = response["session"]["user_id"]
    newcomer = response['session']['new']
    if not newcomer:
        answer = parse.parse_phrase(
            response["session"]["user_id"], 
            request.json["request"]["original_utterance"]
        )
    elif users.get(user_id) is None:
        answer = random.choice(meeting)
    else:
        answer = random.choice(old_meeting)
        
    if isinstance(answer, tuple):
        response['response']['text'] = answer[0]
        response['response']['tts']  = answer[1]
    else:
        response['response']['text'] = answer

    return json.dumps(
        response,
        ensure_ascii=False,
        indent=2
    )
