import logging
import os
import random
import json

from app import VERSION

meeting = [
    'Привет. Давай знакомиться? Я - Хранитель времени. Ты можешь узнать, сколько времени тратишь на задачи. Чтобы узнать подробнее - скажи "Помощь"'
]

old_meeting = [
    'Привет.',
    'С возвращением.',
    'Велкам',
    'Рада снова видеть вас'
]

phrases_first = [
    'Окей, я поняла',
    'Хорошо, отсчёт времени начался',
    'Хорошо, приняла к сведению',
    'Готово!',
    'Окей, начинаю отсчёт'
]

phrases_help = [
    'Активные действия - запусти отсчет, сколько прошло времени (или просто - время), справка.'
]

phrases_about = [
    'Навык находится в бета тестировании', 
    'За иконку спасибо ammatice',
    'Скоро появится открытый исходный код, возможно',
    f'Текущая версия навыка: {VERSION}'
]

phrases_no_user = [
    'Ой, я забыла. Попробуйте начать заново. Достаточно сказать - старт.'
]

panic_phrases = [
    'Не поняла',
    'Что-то не получилось',
    'Давайте попробуем еще раз',
    'Попробуйте сказать как-нибудь иначе',
    'Если ничего не получается, можете попросить меня помочь'
]

pause_phrases = [
    'Скоро будет доступно... (Можно ускорить, написав в личку разработчику)',
    'Возможность скоро появиться (Можно ускорить, написав в личку разработчику)',
    'Фича в процессе (Можно ускорить, написав в личку разработчику)'
]

def help_for_user(user_id):
    '''Give info which help to work with skill'''

    logging.info( f'Request for help_for_user(user_id = {user_id}) was received')
    return random.choice(phrases_help)

def about(user_id):
    '''Give info about skill and how to contact'''

    logging.info( f'Request for about(user_id = {user_id}) was received')
    return random.choice(phrases_about)



def getenv(name):
    return os.environ.get(name)


def save_dict_to_jsonstr(d):
    return json.dumps(d, sort_keys=True, indent=4)

def load_json_to_dict_fromstr(s):
    return json.loads(s)

def load_jsond_from_file(fname):
    return json.load(open(fname))

def save_jsond_to_file(d, fname):
    return json.dump(d, open(fname, 'w'), sort_keys=True, indent=4)


def decsec(num):
    '''declension (?) of seconds'''
    SEC_IN_MIN = 60
    num = num % SEC_IN_MIN
    rem = num % 10

    ans = 'секунд'
    if num == 11 or rem == 0 or rem >= 5:
        ans = 'секунд'
    elif rem == 1:
        ans = 'секунда'
    elif 2 <= rem <= 4 and num not in (12, 13, 14):
        ans = 'секунды'
    return ans


def get_suggests():
    '''return text suggest'''
    
    suggests_text = [
        'Старт',
        'Время?',
        'Помощь'
    ]

    suggests = [
        {'title': suggest, 'hide': True}
        for suggest in suggests_text
    ]

    return suggests