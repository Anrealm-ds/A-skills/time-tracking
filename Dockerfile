FROM python:3.7-alpine

WORKDIR /app
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt
COPY . /app

CMD gunicorn --bind 0.0.0.0:8000 time_tracking:app
