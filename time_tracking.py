#! /usr/bin/env python3

import logging

from app.webhook import app

logging.basicConfig(format = u' %(filename)s[LINE:%(lineno)d] %(levelname)-8s [%(asctime)s]  %(message)s', 
                    level = logging.INFO)


if __name__ == '__main__':
    # blocking function, run flask server
    app.run()
