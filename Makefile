URL=localhost
REGISTRY=registry.gitlab.com/anrealm-ds/a-skills/time-tracking
TAG=latest

test:
	python -m unittest discover -v
run:
	docker-compose up --build
run1:
	docker run -e TT_SAVE_JSON='ttracking/backup_users.json' --rm -it -p 80:8000  ttracker

run2:
	docker run -e TT_SAVE_JSON='ttracking/backup_users.json' -it -p 80:8000  ttracker

flask:
	FLASK_APP=time_tracking.py flask run --host="::" --port="80"
	# python3 time_tracking.py
rung:
	gunicorn -k tornado --bind "[::]:80" time_tracking:app
req:
	pip install -r requirements.txt
build:
	docker build -t ttracker .
ping:
	curl \
		-H "Content-Type: application/json" \
		-X POST \
		-d @tests/simple_request.json \
		localhost
ping2:
	curl \
		-H "Content-Type: application/json" \
		-X POST \
		-d '{"h": "ping"}' \
		localhost

send_again:
	@curl \
		-H "Content-Type: application/json" \
		-X POST \
		-d @tests/empty.json \
		$(URL)

send_howlong:
	@curl \
		-H "Content-Type: application/json" \
		-X POST \
		-d @tests/howlong.json \
		$(URL)
send_start:
	@curl \
		-H "Content-Type: application/json" \
		-X POST \
		-d @tests/start.json \
		$(URL)	

gitlab_build:
	docker build -t $(REGISTRY):$(TAG) .
gitlab_push:
	docker push $(REGISTRY):$(TAG)
