#! /usr/bin/env python
import unittest

from app import tracking

class Ske(unittest.TestCase):

    def test_create_new_user(self):
        tracking.save_time("user_123_321")
        self.assertEqual(type(tracking.users["user_123_321"]), float)

if __name__ == '__main__':
    unittest.main()
 