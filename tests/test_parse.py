import unittest

from app import parse, helper, tracking

class TestParsePhrases(unittest.TestCase):

    def test_about(self):
        self.assertTrue(parse.parse_phrase("1", "Алиса, справка") in helper.phrases_about)
                
    def test_start(self):
        self.assertTrue(parse.parse_phrase("1", "Алиса, стартуем") in helper.phrases_first)

    def test_delta_time(self):
        self.assertTrue(parse.parse_phrase("1", "Алиса, сколько время") in helper.phrases_no_user)

    def test_help(self):
        self.assertTrue(parse.parse_phrase("1", "Алиса, помоги") in helper.phrases_help)

    def test_wrong_phrase(self):
        self.assertTrue(parse.parse_phrase("1", "Алиса, все так быстротечно") in  helper.panic_phrases)

if __name__ == '__main__':
    unittest.main()