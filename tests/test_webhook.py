#! /usr/bin/env python
# -*- coding: utf-8 -*-

import unittest, json
from app import webhook, helper, tracking

webhook.app.config['TESTING'] = True
client = webhook.app.test_client()

def get_response(path_to_json_file):
    """ For getting json from file and sending that json to flask server
    :type path_to_json_file: str
    :return: response from server
    :rtype: <flask.wrappers.Response> 
    """

    with open(path_to_json_file, encoding="utf-8") as f:
        json_from_file = json.load(f)

    response = client.post('/',
        json=json_from_file,
        content_type='application/json',
        follow_redirects=True
    ) 
    return response

class WebhookGoodRequestStart(unittest.TestCase):

    response = get_response("tests/start.json")

    def test_connection(self):
        self.assertEqual(self.response.status_code, 200)

    def test_json_response(self):
        json_response = json.loads(self.response.data.decode("utf-8"))

        self.assertEqual(json_response["session"]["user_id"], "454321")
        self.assertEqual(json_response["version"], "1.0")

    def test_new_user_create(self):
        self.assertEqual("454321" in tracking.users, True)
        self.assertEqual(type(tracking.users["454321"]), float)


class WebhookGoodRequestHowLong(unittest.TestCase):

    response = get_response("tests/howlong.json")

    def test_connection(self):
        self.assertEqual(self.response.status_code, 200)

    def test_new_user_create(self):
        """ When asked how long time, new user is NOT created
        """
        self.assertEqual("33333333333333" in tracking.users, False)

    @unittest.skip('Too haro to understand. Bad idea to check this way...')
    def test_json_response(self):
        json_response = json.loads(self.response.data.decode("utf-8"))

        self.assertEqual(json_response["session"]["user_id"], "33333333333333")
        self.assertEqual(json_response["version"], "1.0")


class WebhookBadRequestBadSession(unittest.TestCase):
    """ Without 'session' part
    """

    response = get_response("tests/bad_without_session.json")

    def test_connection(self):
        self.assertEqual(self.response.status_code, 200)

    def test_wrong_response(self):
        json_response = json.loads(self.response.data.decode("utf-8"))
        self.assertEqual(json_response["response"]["text"], "Это как-то неправильно...")
       
class WebhookBadRequestBadPhrase(unittest.TestCase):
    """ No right phrases
    """

    response = get_response("tests/bad_with_wrong_phrase.json")

    def test_connection(self):
        self.assertEqual(self.response.status_code, 200)

    def test_wrong_phrase(self):
        """ Check, that if phrase not recognized, in response will 
        be phrase from helper.panic_phrases
        """ 

        json_response = json.loads(self.response.data.decode("utf-8"))
        response_text = json_response["response"]["text"]

        self.assertEqual(response_text in helper.panic_phrases, True)

class WebhookBadRequestNotJSON(unittest.TestCase):
    """ if send not json, some another 
    """

    response = client.post('/',
        data="I am string, not JSON",
        follow_redirects=True
    ) 

    def test_connection(self):
        self.assertEqual(self.response.status_code, 200)

    def test_wrong_phrase(self):
        json_response = json.loads(self.response.data.decode("utf-8"))
        self.assertEqual(json_response["response"]["text"], "Это как-то неправильно...")

class WebhookBadRequestNotData(unittest.TestCase):
    """ if send none data 
    """

    response = client.post('/') 

    def test_connection(self):
        self.assertEqual(self.response.status_code, 200)

    def test_wrong_phrase(self):
        json_response = json.loads(self.response.data.decode("utf-8"))
        self.assertEqual(json_response["response"]["text"], "Это как-то неправильно...")

if __name__ == '__main__':
    unittest.main()